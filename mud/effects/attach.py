from .effect import Effect3
from mud.events import AttachWithEvent

class AttachWithEffect(Effect3):
    EVENT = AttachWithEvent
