from .event import Event3

class AttachWithEvent(Event3):
    NAME = "attach-with"

    def perform(self):
        if not self.object.has_prop("attachable"):
            self.fail()
            return self.inform("attach-with.failed")
        self.inform("attach-with")
